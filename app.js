const Koa = require('koa')
const mysql = require('mysql')
const co = require('co-mysql')
const bodyParser = require('koa-bodyparser') //post 数据解析
var cors = require('koa2-cors') //
let router = require('./router/index')
const { createProxyMiddleware } = require('http-proxy-middleware')

const app = new Koa()

// let connect = mysql.createPool({
//   host: '192.168.0.148',
//   port: 3306,
//   user: 'root',
//   password: '123456',
//   database: 'book',
// })
// app.use(
//   createProxyMiddleware('/api', {
//     target: 'http://192.168.0.148:3001',
//     changeOrigin: true,
//   }),
// )
app.use(
  cors({
    origin: function (ctx) {
      console.log(ctx)
      if (ctx.url === '/test') {
        return '*'
      }
      return 'http://localhost:3000'
    },
    exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
    maxAge: 5,
    credentials: true,
    allowMethods: ['GET', 'POST', 'DELETE'],
    allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
  }),
)
app.context.db = co(connect)
app.use(bodyParser())
app.use(router.routes())

app.listen(3001, function () {
  console.log('serve start')
})

// Get请求时，Get的参数通过ctx.query[key]
// Post的请求数据是存在body的 koa-bodyparser
// 权限验证可以用 koa-session
