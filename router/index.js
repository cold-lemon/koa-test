const Router = require('koa-router')

const router = new Router()

router.all('/', (ctx) => {
  ctx.body = 'Hello,Koa'
})
router.get('/api/queryUserPermission', async (ctx) => {
  ctx.body = await ctx.db.query('SELECT * FROM tb_role')
  console.log(ctx.body)
})
router.get('/api/queryMenu', async (ctx) => {
  ctx.body = await ctx.db.query('SELECT * FROM tb_menu')
})

module.exports = router
